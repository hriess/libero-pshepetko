package org.eevolution.process;


/**********************************************************************
 * This file is part of iDempiere ERP Bazaar                          * 
 * http://www.idempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Victor Perez	                                      * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Peter Shepetko               									  *
 *                                                                    *
 * Sponsors:                                                          *
 *  - iPalich          										          *
 **********************************************************************/


import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;

import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.idempiere.model.I_DD_OrderLine;
import org.idempiere.model.MDDOrder;
import org.idempiere.model.MDDOrderLine;

/**
 *	Generate Inventory Move Document based Distribution Order Lines and the Smart Browser Filter  
 *  @author peter.shepetko
 *  @version $Id: $
 */
public class GenerateIMfromDO extends SvrProcess
{	
	
	/**	The current Shipment	*/
	private MMovement		m_movement = null;	
	private MMovement		m_movement2 = null;	
	private static int p_WTransitOrg = 0;
	
	protected int p_M_Locator_ID = 0;
	protected int p_M_LocatorTo_ID = 0;	
	protected int source_M_LocatorTo_ID = 0;

	
	/** Record ID */
	protected int p_Record_ID = 0;	
	protected String p_DocAction = null;
	protected Timestamp p_ShipDate = null;
	protected Timestamp p_PickDate = null;
	protected int p_C_DocType_ID = 0;
	protected String p_DeliveryRule = null;
	protected String p_POReference = null;
	
;
	
	/**
	 * 	Get Parameters
	 */
	protected void prepare ()
	{
		
		p_Record_ID = getRecord_ID();
		for (ProcessInfoParameter para:getParameter())
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("M_Locator_ID"))
			{
				p_M_Locator_ID =  para.getParameterAsInt();
			}
			else if (name.equals("M_LocatorTo_ID"))
			{
				p_M_LocatorTo_ID =  para.getParameterAsInt();
			}
			
			
			
			
			else if (name.equals("ShipDate"))
			{
				p_ShipDate =  (Timestamp)para.getParameter();
			}
			else if (name.equals("PickDate"))
			{
				p_PickDate =  (Timestamp)para.getParameter();
			}
			else if (name.equals("POReference"))
			{
				p_POReference =  (String)para.getParameter();
			}

			else if (name.equals("DeliveryRule"))
			{
				p_DeliveryRule =  (String)para.getParameter();
			}
			else if (name.equals("C_DocType_ID"))
			{
				p_C_DocType_ID = para.getParameterAsInt();
			}
			else if (name.equals("DocAction"))
			{
				p_DocAction = (String)para.getParameter();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	/**
	 * 	Process - Generate Export Format
	 *	@return info
	 */
	@SuppressWarnings("unchecked")
	protected String doIt () throws Exception
	{
		String _result="";int seq = 10;
		int old_DD_Order_ID=0;
		//red1 works with IDEMPIERE-1711 Info Window Process that saves Selected records in T_Selection table
		String whereClause = "EXISTS (SELECT T_Selection_ID FROM T_Selection WHERE  T_Selection.AD_PInstance_ID=? " +
				"AND T_Selection.T_Selection_ID=DD_OrderLine.DD_OrderLine_ID)";		
		
		Collection <MDDOrderLine> dolines = new Query(getCtx(), I_DD_OrderLine.Table_Name, whereClause, get_TrxName())
										.setOrderBy("DD_Order_ID")
										.setClient_ID()
										.setParameters(new Object[]{getAD_PInstance_ID()	})
										.list();
		
		for (MDDOrderLine doline : dolines)
		{
			MDDOrder order = new MDDOrder (getCtx(), doline.getDD_Order_ID(), get_TrxName());
			_result+="|DO_ID="+doline.getDD_Order_ID();
			
			//get supply source warehouse and locator
			MWarehouse p_WTransit = MWarehouse.get(getCtx(), order.getM_Warehouse_ID());	
			MLocator WTlocator = p_WTransit.getDefaultLocator();
			 p_WTransitOrg=WTlocator.getAD_Org_ID();
			
			//create new InventoryMove
			if (old_DD_Order_ID != doline.getDD_Order_ID()){
			m_movement = createMovement(order, doline.getDatePromised());
			m_movement2 = createMovement2(order, doline.getDatePromised());
				if (!m_movement.save())
					throw new IllegalStateException("Could not create Inventory Move1!");
				if (!m_movement2.save())
					throw new IllegalStateException("Could not create Inventory Move2!");
			  seq = 10; 
			}
							
			
			//create new InventoryMove1 Lines
			MMovementLine mline = new MMovementLine (m_movement);
			mline.setLine(seq);
			mline.setAD_Org_ID(p_WTransitOrg);
			mline.setM_Product_ID(doline.getM_Product_ID());
			mline.setM_AttributeSetInstance_ID(doline.getM_AttributeSetInstance_ID());			
			mline.setM_Locator_ID(doline.getM_Locator_ID());
			mline.setM_LocatorTo_ID(WTlocator.getM_Locator_ID());
			mline.setDD_OrderLine_ID(doline.getDD_OrderLine_ID());
			mline.setM_Product_ID(doline.getM_Product_ID());
			mline.setM_AttributeSetInstance_ID(doline.getM_AttributeSetInstance_ID());
			mline.setM_AttributeSetInstanceTo_ID(doline.getM_AttributeSetInstanceTo_ID());
			mline.setMovementQty(doline.getQtyOrdered());
			mline.saveEx();

			//create new InventoryMove2 Lines
			MMovementLine mline2 = new MMovementLine (m_movement2);
			mline2.setLine(seq);
			mline2.setAD_Org_ID(order.getAD_Org_ID());
			mline2.setM_Product_ID(doline.getM_Product_ID());
			mline2.setM_AttributeSetInstance_ID(doline.getM_AttributeSetInstance_ID());			
			mline2.setM_Locator_ID(WTlocator.getM_Locator_ID());
			mline2.setM_LocatorTo_ID(doline.getM_LocatorTo_ID());
			mline2.setDD_OrderLine_ID(doline.getDD_OrderLine_ID());
			mline2.setM_Product_ID(doline.getM_Product_ID());
			mline2.setM_AttributeSetInstance_ID(doline.getM_AttributeSetInstance_ID());
			mline2.setM_AttributeSetInstanceTo_ID(doline.getM_AttributeSetInstanceTo_ID());
			mline2.setMovementQty(doline.getQtyOrdered());
			mline2.saveEx();
				seq ++; old_DD_Order_ID=doline.getDD_Order_ID(); 
		}
		//return "@DocumentNo@ " + outbound.getDocumentNo();
 		return "���������� ����������� �������!";
	}
	
	private static MMovement existMovement(int old_DD_Order_ID)
	{
		MMovement move = new MMovement(null,old_DD_Order_ID, null);
		return move; 
	}
	
	private static MMovement createMovement(MDDOrder order, Timestamp movementDate)
	{
		MMovement move = new MMovement(order.getCtx(), 0, order.get_TrxName());
	//	move.setC_BPartner_ID (order.getC_BPartner_ID());
	//	move.setC_BPartner_Location_ID (order.getC_BPartner_Location_ID());	//	shipment address
		move.setAD_User_ID(order.getAD_User_ID());
		move.setC_DocType_ID(1000069); 
		move.setAD_Org_ID(p_WTransitOrg);
		//	Default - Today
		if (movementDate != null)
			move.setMovementDate (movementDate);
			
		//	Copy from Order
		move.setDD_Order_ID(order.getDD_Order_ID());
		move.setDeliveryRule (order.getDeliveryRule());
		move.setDeliveryViaRule (order.getDeliveryViaRule());
		move.setM_Shipper_ID(order.getM_Shipper_ID());
		move.setFreightCostRule (order.getFreightCostRule());
		move.setFreightAmt(order.getFreightAmt());
		move.setSalesRep_ID(order.getSalesRep_ID());
		move.setC_Activity_ID(order.getC_Activity_ID());
		move.setC_Campaign_ID(order.getC_Campaign_ID());
		move.setC_Charge_ID(order.getC_Charge_ID());
		move.setC_Project_ID(order.getC_Project_ID());
		move.setSalesRep_ID(order.getSalesRep_ID());
		move.setAD_OrgTrx_ID(order.getAD_OrgTrx_ID());
		move.setUser1_ID(order.getUser1_ID());
		move.setUser2_ID(order.getUser2_ID());
		move.setPriorityRule(order.getPriorityRule());	
		move.setDescription("Generated by DRP!");
		move.setDocStatus(MDDOrder.DOCSTATUS_Drafted);
		return move; 
	}

	private static MMovement createMovement2(MDDOrder order, Timestamp movementDate)
	{
		MMovement move = new MMovement(order.getCtx(), 0, order.get_TrxName());
	//	move.setC_BPartner_ID (order.getC_BPartner_ID());
	//	move.setC_BPartner_Location_ID (order.getC_BPartner_Location_ID());	//	shipment address
		move.setAD_User_ID(order.getAD_User_ID());
		move.setC_DocType_ID(1000069); 
		move.setAD_Org_ID(order.getAD_Org_ID());
		//	Default - Today
		if (movementDate != null)
			move.setMovementDate (movementDate);
			
		//	Copy from Order
		move.setDD_Order_ID(order.getDD_Order_ID());
		move.setDeliveryRule (order.getDeliveryRule());
		move.setDeliveryViaRule (order.getDeliveryViaRule());
		move.setM_Shipper_ID(order.getM_Shipper_ID());
		move.setFreightCostRule (order.getFreightCostRule());
		move.setFreightAmt(order.getFreightAmt());
		move.setSalesRep_ID(order.getSalesRep_ID());
		move.setC_Activity_ID(order.getC_Activity_ID());
		move.setC_Campaign_ID(order.getC_Campaign_ID());
		move.setC_Charge_ID(order.getC_Charge_ID());
		move.setC_Project_ID(order.getC_Project_ID());
		move.setSalesRep_ID(order.getSalesRep_ID());
		move.setAD_OrgTrx_ID(order.getAD_OrgTrx_ID());
		move.setUser1_ID(order.getUser1_ID());
		move.setUser2_ID(order.getUser2_ID());
		move.setPriorityRule(order.getPriorityRule());	
		move.setDescription("Generated by DRP!");
		move.setDocStatus(MDDOrder.DOCSTATUS_Drafted);
		return move; 
	}

	
}
